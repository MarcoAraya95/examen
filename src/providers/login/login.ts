import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {
    rut : null;
    role : null;
    email : null; 
    apiKey : null;

  constructor(public http: HttpClient) {
    console.log('Hello LoginProvider Provider');
  }

  login(rut:string, password: string)
  {
    let url = 'https://api.sebastian.cl/academia/api/v1/authentication/authenticate';
    let info = {rut: rut, password: password};
    return this.http.post(url,info).pipe(tap((data : any) => {
      this.rut = data.rut;
      this.role = data.role;
      this.apiKey = data.apiKey;
      this.email = data.email;
      console.log("====== From login provider ======");
      console.log("====== Login provider done ====== ")
    }
    ))
  };
      

}




  
