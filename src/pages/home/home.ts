import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CursosProvider } from '../../providers/cursos/cursos'
import { LoginProvider } from '../../providers/login/login';
import { AlertController, LoadingController } from 'ionic-angular';
import { Chart } from 'chart.js';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  chart: any = null;
  nombrecursos = [];

  cursos = [];
  cursosbuffer = [];
  tambuffer = 100;

  codigo: string;
  years = [];
  media: number[];
  promedio = [];
  year = [];

  promediog = [];
  yearg = [];

  añografico: any[];

  guardarpromedio: any[];


  constructor(public navCtrl: NavController,
              public cursosprovider: CursosProvider,
              public loginprovider: LoginProvider,
              public loadingCtrl: LoadingController,
              public alertcontrol: AlertController
              ) {

  }
  

  

  ngOnInit()
  {

    let canvas = document.getElementById("canvas");
    this.chart = new Chart(canvas, {
        type: 'line',
        data: {
          datasets: [{
              label: 'Curso',
              data: 0 ,
              borderColor: "#3e95cd"
            },
            {
              label: 'General',
              data: 0,
              borderColor: "#3cba9f"
            }
          ],
            options:{
              title:{
                display: true,
                text: 'Promedio por Años'
              }
            }
        }
    });
    /*
    let loader = this.loadingCtrl.create({
      content: "Cargando informacion..."
  });

  loader.present();
    console.log(this.loginprovider.apiKey);
    this.cursosprovider
        .listado(this.loginprovider.apiKey).subscribe((data:any)=>{
          console.log(data);
          data.forEach(fila => {
            this.cursos.push({
              nombre: fila.name,
              code: fila.code,
              id: fila.id
            })
          }
        );
      })
      loader.dismiss();
      */

 

      console.log(this.loginprovider.apiKey);
    this.cursosprovider
        .listado(this.loginprovider.apiKey).subscribe((data:any)=>{
          console.log(data);
          data.forEach(fila => {
                this.cursos.push({
                  nombre: fila.name,
                  code: fila.code,
            })
          this.cursosbuffer = this.cursos.slice(0,this.tambuffer);
          }
          );
      })
  }

    


  cambiar(code: string)
      {
        /*
        console.log("sdfdsfds");
        console.log(code);
        this.cursosprovider
        .añoscurso(this.loginprovider.apiKey, code).subscribe((data: [any])=>{
          console.log(data);
          this.promedio = this.promedios(data);
          this.year = this.yearscurso(data);
                },
                  error=>{
                  console.log(error);
                  this.alertcontrol.create(
                    {
                      title: "Error",
                      message: error.error.message
                    }
                  )
                  .present();
                });
          
        this.cursosprovider
        .añosgeneral(this.loginprovider.apiKey).subscribe((data: [any])=>{
          console.log(data);
          this.promediog = this.promediosg(data);
          this.yearg = this.yearsg(data);
        }, error=>{
          console.log(error);
          this.alertcontrol.create(
            {
              title: "Error",
              message: error.error.message
            }
          )
          .present();
        });
        */


       console.log(code);
       this.cursosprovider
       .añoscurso(this.loginprovider.apiKey, code).subscribe((data: [any])=>{
         console.log(data);
         let promedio = this.promedios(data);
         let year = this.yearscurso(data);
         this.cursosprovider
       .añosgeneral(this.loginprovider.apiKey).subscribe((data2: [any])=>{
         console.log(data2);
         let promediog = this.promediosg(data2);
         let yearg = this.yearsg(data2);

         this.guardarpromedio = this.guardarpromedios(promedio, year, yearg);
         if(this.chart)
         {
           this.chart.clear();
           this.chart.destroy();
         }
                let lineas = document.getElementById("canvas");
                this.chart = new Chart(lineas, {
                    type: 'line',
                    data: {
                        labels: yearg.reverse(),
                        datasets: [{
                            label: 'Curso',
                            data: this.guardarpromedio.reverse(),
                            borderColor: "#3e95cd"
                          },
                          {
                            label: 'General',
                            data: promediog.reverse(),
                            borderColor: "#3cba9f"
                          }
                        ],
                        options:{
                          title:{
                            display: true,
                            text: 'Promedio por Años'
                          }
                        }
                    }
                });

                

                
                
       }, error=>{
         console.log(error);
         this.alertcontrol.create(
           {
             title: "Error",
             message: error.error.message
           }
         )
         .present();
       });
               },
                 error=>{
                 console.log(error);
                 this.alertcontrol.create(
                   {
                     title: "Error",
                     message: error.error.message
                   }
                 )
                 .present();
               });
         /*
       this.cursosprovider
       .añosgeneral(this.loginprovider.apiKey).subscribe((data: [any])=>{
         console.log(data);
         this.promediog = this.promediosg(data);
         this.yearg = this.yearsg(data);
       }, error=>{
         console.log(error);
         this.alertcontrol.create(
           {
             title: "Error",
             message: error.error.message
           }
         )
         .present();
       });
          
          this.guardarpromedio = this.guardarpromedios(this.promedio, this.year, this.yearg);

                let canvas = document.getElementById("canvas");
                this.chart = new Chart(canvas, {
                    type: 'line',
                    data: {
                        labels: this.year.reverse(),
                        datasets: [{
                            label: 'Curso',
                            data: this.promedio.reverse(),
                            borderColor: "#3e95cd"
                          },
                          {
                            label: 'General',
                            data: this.guardarpromedio.reverse(),
                            borderColor: "#3cba9f"
                          }
                        ],
                        options:{
                          title:{
                            display: true,
                            text: 'Promedio por Años'
                          }
                        }
                    }
                });

          console.log("/--canvas--");*/
      }

 



promedios(data : [any]) {
  let promedio = [];
  data.forEach(data => {
    promedio.push(data.average);
});

  return (promedio);
}

yearscurso(data : [any]) {
  let years = [];
  data.forEach(data => {
    years.push(data.year);
});

  return (years);
}

promediosg(data : [any])
{
  let promediog = [];
  data.forEach(data => {
    promediog.push(data.average);
});  
  return (promediog);
}


yearsg(data : [any]) {
  let yearsg = [];
  data.forEach(data => {
    yearsg.push(data.year);
});

  return (yearsg);
}


guardarpromedios(promedio: any[], year: any[], yearg: any[])
{
  console.log("holi k ase");
  let guardarpromedios = [];
  let posicion:number =  0;
  let posicion2:number = 0;
  year.forEach(data1 => {
    yearg.forEach(data2 => {
      if(data1 == data2)
      {
        guardarpromedios[posicion2] = promedio[posicion];
        console.log("----guardarprom----");
        console.log(guardarpromedios[posicion2]);
        console.log(posicion2);
        console.log("---guardado iguales ---");
      }
      if(!guardarpromedios[posicion2])
      {
        guardarpromedios[posicion2] = 1;
      }
      posicion2++;
    }
    );
    posicion++;
    posicion2 = 0;
  });

return (guardarpromedios);
}


}
  
/*
  arreglo(data: any[]) {
    this.data = data;
    this.nombrecursos = (this.data.map(c => c.name));
}*/



