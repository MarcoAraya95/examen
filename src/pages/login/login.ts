import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';

import { HomePage } from '../home/home';
import { LoginProvider } from '../../providers/login/login'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myForm: FormGroup;


  constructor(public navCtrl: NavController,
              public loginprovider: LoginProvider,
              public formBuilder: FormBuilder,
              public alertcontrol: AlertController) {

                this.myForm = this.formBuilder.group({
                  rut: ['', Validators.compose([Validators.pattern('^([0-9]{1}|[0-9]{2})(\.[0-9]{3}\.[0-9]{3}-)([a-zA-Z]{1}$|[0-9]{1}$)'),
                  Validators.required])],
                  password: ['', Validators.required]
                });
  }


  login() {
    this.loginprovider
        .login(this.myForm.value.rut,this.myForm.value.password).subscribe((data:any)=>{
        console.log(data);
        localStorage.setItem("apiKey", data.apikey);
        localStorage.setItem("role", data.role);
        localStorage.setItem("email", data.email);
        localStorage.setItem("rut", data.rut);
        this.navCtrl.push(HomePage, localStorage.getItem("apiKey"));
        }, error=>{
          console.log(error);
          this.alertcontrol.create(
            {
              title: "Error",
              message: error.error.message
            }
          )
          .present();
        })
  }

}

